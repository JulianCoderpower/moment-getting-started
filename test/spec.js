var moment = require('moment');
var expect = require('expect.js');
var discovery = require('../sources/getting-started.js');

describe('How to format date with Moment.js', function(){
    it('should return a string', function(){
        var result = discovery(moment);
        expect(result).to.be.a('string');
        expect(result).to.be.ok();
    });
    it('should return the right date', function(){
        var result = discovery(moment);
        expect(result).to.be.equal('Sunday, April 3, 2016 12:00 AM');
    });
});