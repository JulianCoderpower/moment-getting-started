var moment = require('moment');
var expect = require('expect.js');
var discovery = require('../sources/getting-started.js');

describe('How to format date with Moment.js', function(){
    it('should return a string', function(){
        expect(discovery(moment)).to.be.a('string');
        expect(discovery(moment)).to.be.ok();
    });
    it('should return the right date', function(){
        expect(discovery(moment)).to.be.equal('Sunday, April 3, 2016 12:00 AM');
    });
});